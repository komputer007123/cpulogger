
-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Июн 23 2016 г., 15:45
-- Версия сервера: 10.0.22-MariaDB
-- Версия PHP: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `u280312026_cpudb`
--

-- --------------------------------------------------------

--
-- Структура таблицы `CpuBlock`
--

CREATE TABLE IF NOT EXISTS `CpuBlock` (
  `idCpuBlock` int(11) NOT NULL AUTO_INCREMENT,
  `idUser` int(11) NOT NULL,
  `computerName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `beginDate` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endDate` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `interval` int(11) NOT NULL,
  PRIMARY KEY (`idCpuBlock`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=25 ;

--
-- Дамп данных таблицы `CpuBlock`
--

INSERT INTO `CpuBlock` (`idCpuBlock`, `idUser`, `computerName`, `beginDate`, `endDate`, `interval`) VALUES
(21, 1, 'ANDRONHPWIN', '2016-06-23 22:40:55', '2016-06-23 22:40:57', 1),
(22, 1, 'ANDRONHPWIN', '2016-06-23 22:41:03', '2016-06-23 22:41:09', 1),
(23, 1, 'ANDRONHPWIN', '2016-06-23 22:41:24', '2016-06-23 22:41:35', 1),
(24, 1, 'ANDRONHPWIN', '2016-06-23 22:43:11', '2016-06-23 22:43:17', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `CpuData`
--

CREATE TABLE IF NOT EXISTS `CpuData` (
  `idCpuBlock` int(11) NOT NULL,
  `idCpuData` int(11) NOT NULL AUTO_INCREMENT,
  `currentCpuUsage` int(11) NOT NULL,
  PRIMARY KEY (`idCpuData`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=200 ;

--
-- Дамп данных таблицы `CpuData`
--

INSERT INTO `CpuData` (`idCpuBlock`, `idCpuData`, `currentCpuUsage`) VALUES
(24, 199, 8),
(24, 198, 7),
(24, 197, 9),
(24, 196, 7),
(24, 195, 5),
(24, 194, 0),
(23, 193, 8),
(23, 192, 4),
(23, 191, 6),
(23, 190, 8),
(23, 189, 4),
(23, 188, 6),
(23, 187, 5),
(23, 186, 7),
(23, 185, 8),
(23, 184, 6),
(23, 183, 7),
(22, 182, 5),
(22, 181, 5),
(22, 180, 4),
(22, 179, 5),
(22, 178, 6),
(22, 177, 6),
(21, 176, 7),
(21, 175, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `User`
--

CREATE TABLE IF NOT EXISTS `User` (
  `idUser` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idUser`),
  UNIQUE KEY `login` (`login`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `User`
--

INSERT INTO `User` (`idUser`, `login`, `password`) VALUES
(1, 'test', '*23AE809DDACAF96AF0FD78ED04B6A265E05AA257'),
(2, 'test2', '*23AE809DDACAF96AF0FD78ED04B6A265E05AA257'),
(3, 'test3', '*23AE809DDACAF96AF0FD78ED04B6A265E05AA257'),
(4, 'test4', '*23AE809DDACAF96AF0FD78ED04B6A265E05AA257');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
