<?php
	class Api {
		public function eho( $arg ) {
			return $arg;
		}
		
		public function __construct( $db_uri, $db_user, $db_password, $db_name ) {
			$this->db_uri = $db_uri;
			$this->db_user = $db_user;
			$this->db_password = $db_password;
			$this->db_name = $db_name;
			
			$this->db = mysql_connect( $this->db_uri, $this->db_user, $this->db_password );
			if (!$this->db) {
				die('db_connection_error: ' . mysql_error());
			}
			$db_selected = mysql_select_db($this->db_name, $this->db);
			if (!$db_selected) {
				die ('db_selecting_error: ' . mysql_error());
			}
			mysql_set_charset('utf8');
		}
		
		public function __destruct() {
			mysql_close( $this->db );
		}
		
/* 		public function get_all_info()
		{
			$query = "SELECT * FROM `Info`";
			$result = mysql_query( $query );
			while ($row = mysql_fetch_array($result, MYSQL_NUM)) {
				$ret[] = $row;
			}
			return $ret;
		} */
		
		public function login( $login, $password ) {
			$login = mysql_real_escape_string( $login );
			$password = mysql_real_escape_string( $password );
			
			$result = mysql_query( "SELECT `idUser` FROM `User` WHERE `login` = '$login' AND `password` = PASSWORD('$password')" );
			if (mysql_errno() == 0) {
				$result = mysql_fetch_array( $result, MYSQL_NUM );
				if ($result == false) {
					return 0;
				} else {
					return (int)$result[0];
				}
			} else {
				return -1;
			}
		}
		
		public function register( $login, $password ) {
			$login = mysql_real_escape_string( $login );
			$password = mysql_real_escape_string( $password );
			
			$query = "INSERT INTO `User` (`login`,`password`) VALUES ('$login',PASSWORD('$password'))";
			$result = mysql_query( $query );
			if ($result == false)
			{
				return -1;
			}
			
			return (int)mysql_insert_id();
		}
		
		public function get_all_names()
		{
			$query = "SELECT `computerName`, `idUser` FROM `CpuBlock`";
			$result = mysql_query( $query );
			$numResults = mysql_num_rows($result);
			if ($numResults > 0) {
			   	while ($row = mysql_fetch_array($result, MYSQL_NUM)) {
					$ret[] = $row;
				}
 				return $ret;
			} else {
				return array(array()); 
			}
		}
		
		public function get_list( $idUser, $computerName )
		{
			$idUser = mysql_real_escape_string($idUser);
			$computerName = mysql_real_escape_string($computerName);
			$query = "SELECT `idCpuBlock`, `beginDate`, `endDate`, `idUser` FROM `CpuBlock` WHERE `idUser` = '$idUser' AND `computerName` = '$computerName'";
			$result = mysql_query( $query );
			$numResults = mysql_num_rows($result);
				if ($numResults > 0) {
				while ($row = mysql_fetch_array($result, MYSQL_NUM)) {
					$ret[] = $row;
				}
				return $ret;
			} else {
				return array(array());
			}
		}
		
		public function get_info( $idCpuBlock )
		{
			$idCpuBlock = mysql_real_escape_string($idCpuBlock);
			
			$interval_query = "SELECT `interval` FROM `CpuBlock` WHERE `idCpuBlock` = '$idCpuBlock'";
			$interval_result = mysql_query( $interval_query );
			$data[] = mysql_fetch_array($interval_result, MYSQL_NUM);
			
			$data_query = "SELECT `currentCpuUsage` FROM `CpuData` WHERE `idCpuBlock` = '$idCpuBlock' ORDER BY `idCpuData`";
			$data_result = mysql_query( $data_query );
			while ($row = mysql_fetch_array($data_result, MYSQL_NUM)) {
				$data[] = $row;
			}
			
			return $data;
		}
		
		/*
		$auth_res = $this->auth_teacher( $auth[0], $auth[1] );
			if ($auth_res['error'] === '') {
				$idTeacher = $auth_res['result'];
				$confirm = $this->get_confirm( $idDiploma, $idTeacher );
				if ($confirm['error'] === '') {
					if (count( $confirm['result'] ) === 1) {
						return $this->_update('Mark', compact( 'rabota', 'zapiska', 'doklad', 'vopros' ), compact( 'idDiploma', 'idTeacher' ) );
					} else {
						return array( 'error' => 'Оценки подтверждены секретарем. Редактирование невозможно' );
					}
				} else {
					return $confirm;
				}
			} else {
				return $auth_res;
			}
		*/
		
		public function delete_all_cpu_info($auth)
		{			
			$idUser = $this->login( $auth[0], $auth[1] );
			if ($idUser > 0) {			
				$cpu_block_query = "SELECT `idCpuBlock` FROM `CpuBlock` WHERE `idUser` = '$idUser'";
				$cpu_block_result = mysql_query( $cpu_block_query );
				while ($row = mysql_fetch_array($cpu_block_result, MYSQL_NUM)) {
					$data_delete_query = "DELETE FROM `CpuData` WHERE `idCpuBlock` = '$row[0]'";
					mysql_query($data_delete_query);
					
					$cpu_block_delete_query = "DELETE FROM `CpuBlock` WHERE `idCpuBlock` = '$row[0]'";
					mysql_query($cpu_block_delete_query);
				}
				
				return true;
			} else {
				return false;
			}
		}
		
		public function add_info($auth, $computerName, $beginDate, $endDate, $interval, $cpu_usage_array)
		{
			$computerName = mysql_real_escape_string($computerName);
			$beginDate = mysql_real_escape_string($beginDate);
			$endDate = mysql_real_escape_string($endDate);
			$interval = mysql_real_escape_string($interval);
			
			$idUser = $this->login( $auth[0], $auth[1] );
			if ($idUser > 0) {
				$query = "INSERT INTO `CpuBlock` (`idUser`,`computerName`,`beginDate`,`endDate`, `interval`) VALUES ('$idUser','$computerName','$beginDate', '$endDate', '$interval')";
				$result = mysql_query( $query );
				if ($result ==false)
				{
					return -1;
				}
				
				$idCpuBlock = mysql_insert_id();
				
				foreach ($cpu_usage_array as $value)
				{
					$query = "INSERT INTO `CpuData` (`idCpuBlock`,`currentCpuUsage`) VALUES ('$idCpuBlock','$value')";
					$result = mysql_query( $query );
					if ($result ==false)
					{
						return -1;
					}
				}
				return $idCpuBlock;
			} else {
				return -1;
			}
		}
    }
?>
