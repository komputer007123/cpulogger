﻿namespace Cpulogger.Client
{
    /// <summary>
    /// Класс, реализующий паттерн "Одиночка".
    /// </summary>
    public class Singleton<T> where T : class, new()
    {
        /// <summary>
        /// Экземпляр-одиночка.
        /// </summary>
        public static T Instance => SingletonCreator<T>.CreatorInstance;

        /// <summary>
        /// Нужен, чтобы предотвратить создание экземпляра класса Singleton.
        /// </summary>
        protected Singleton()
        {

        }

        /// <summary>
        /// Фабрика для отложенной инициализации экземпляра класса.
        /// </summary>
        private static class SingletonCreator<TS> where TS : class, new()
        {
            /// <summary>
            /// Необходим для организации критической секции.
            /// </summary>
            // ReSharper disable once StaticMemberInGenericType
            private static readonly object Locker = new object();

            /// <summary>
            /// Экземпляр-одиночка.
            /// </summary>
            private static volatile TS _instance;

            /// <summary>
            /// Экземпляр-одиночка.
            /// </summary>
            public static TS CreatorInstance
            {
                get
                {
                    if (_instance == null)
                    {
                        lock (Locker)
                        {
                            if (_instance == null)
                            {
                                _instance = new TS();
                            }
                        }
                    }

                    return _instance;
                }
            }
        }
    }
}
