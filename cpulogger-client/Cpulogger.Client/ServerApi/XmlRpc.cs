using System;
using System.Collections;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Cpulogger.Client.ServerApi
{
    public class XmlRpc
    {
        private readonly string _serviceUrl;

        public XmlRpc(string serviceUrl)
        {
            _serviceUrl = serviceUrl;
        }
        
        public object Call(string methodName, params object[] args)
        {
            var request = (HttpWebRequest) WebRequest.Create(_serviceUrl);
            request.UserAgent = "XML-RPC Client (Xamarin)";
            request.Method = "POST";
            request.ContentType = "text/xml";
            request.Timeout = 30000;

            SerializeCall(request, methodName, args);

            return ProcessResponse(request);
        }

        public Task<object> CallAsync(string methodName, params object[] args)
        {
            return Task.Factory.StartNew(() => Call(methodName, args));
        }

        private static object ProcessResponse(HttpWebRequest request)
        {
            var xmlResponse = new XmlTextReader(request.GetResponse().GetResponseStream());

            var xmlDoc = new XmlDocument();
            xmlDoc.Load(xmlResponse);

            var responseNode = xmlDoc.SelectSingleNode("//methodResponse/params");
            var faultNode = xmlDoc.SelectSingleNode("//methodResponse/fault");

            if (responseNode != null)
            {
                var valueNode = responseNode.SelectSingleNode("param/value").ChildNodes[0];
                return DeserializeNode(valueNode);
            }

            if (faultNode != null)
            {
                throw DeserializeFault(faultNode.SelectSingleNode("value").ChildNodes[0]);
            }

            return null;

        }

        private static XmlRpcFault DeserializeFault(XmlNode node)
        {
            var htbFault = (Hashtable) DeserializeNode(node);
            return new XmlRpcFault((int) htbFault["faultCode"], (string) htbFault["faultString"]);
        }

        private static object DeserializeNode(XmlNode node)
        {
            if (node == null)
                return null;
            switch (node.Name.ToLower())
            {
                case "int":
                case "i4":
                    return int.Parse(node.InnerText);
                case "boolean":
                    return int.Parse(node.InnerText) == 1;
                case "string":
                    return node.InnerText;
                case "double":
                    return float.Parse(node.InnerText);
                case "dateTime.iso8601":
                    return DateTime.Parse(node.InnerText);
                case "base64":
                    return Convert.FromBase64String(node.InnerText);
                case "array":
                    return ParseArray(node);
                case "struct":
                    return ParseStruct(node);
                default:
                    return node.InnerText;
            }
        }

        private static ArrayList ParseArray(XmlNode node)
        {
            var array = new ArrayList();

            var values = node.SelectNodes("data/value");
            foreach (XmlNode val in values)
                array.Add(DeserializeNode(val.ChildNodes[0]));

            return array;

        }

        private static Hashtable ParseStruct(XmlNode node)
        {
            var hash = new Hashtable();

            var members = node.SelectNodes("member");
            foreach (XmlNode member in members)
                hash[member.SelectSingleNode("name").InnerText] =
                    DeserializeNode(member.SelectSingleNode("value").ChildNodes[0]);

            return hash;
        }

        private static void SerializeCall(HttpWebRequest request, string methodName, object[] args)
        {
            var stream = request.GetRequestStream();
            using (var writer = new XmlTextWriter(stream, Encoding.ASCII))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("methodCall");
                writer.WriteElementString("methodName", methodName);

                writer.WriteStartElement("params");
                if (args != null)
                    foreach (var arg in args)
                    {
                        writer.WriteStartElement("param");
                        SerializeParam(writer, arg);
                        writer.WriteEndElement();
                    }

                writer.WriteEndElement();
                writer.WriteEndElement();
            }


        }

        private static void SerializeParam(XmlTextWriter writer, object arg)
        {
            writer.WriteStartElement("value");

            if (arg != null)
            {
                var type = arg.GetType().ToString();
                switch (type)
                {
                    case "System.Boolean":
                        writer.WriteElementString("boolean", (bool) arg ? "1" : "0");
                        break;
                    case "System.String":
                        writer.WriteElementString("string", arg.ToString());
                        break;
                    case "System.Int16":
                    case "System.Int32":
                    case "System.Int64":
                        writer.WriteElementString("int", arg.ToString());
                        break;
                    case "System.Decimal":
                    case "System.Double":
                        writer.WriteElementString("double", arg.ToString());
                        break;
                    case "System.Collections.ArrayList":
                        writer.WriteStartElement("array");
                        writer.WriteStartElement("data");
                        var array = (ArrayList) arg;
                        foreach (var item in array)
                            SerializeParam(writer, item);
                        writer.WriteEndElement();
                        writer.WriteEndElement();
                        break;
                    case "System.Collections.Hashtable":
                        writer.WriteStartElement("struct");
                        var hash = (Hashtable) arg;
                        foreach (string key in hash.Keys)
                        {
                            writer.WriteStartElement("member");
                            writer.WriteElementString("name", key);
                            SerializeParam(writer, hash[key]);
                            writer.WriteEndElement();
                        }
                        writer.WriteEndElement();
                        break;
                    default:
                        writer.WriteString(arg.ToString());
                        break;
                }
            }
            writer.WriteEndElement();
        }

        public class XmlRpcFault : Exception
        {
            public XmlRpcFault(int faultCode, string faultMessage)
                : base("Server returned a fault exception: [" + faultCode + "] " + faultMessage)
            {
                FaultCode = faultCode;
                FaultMessage = faultMessage;
            }

            public int FaultCode { get; }

            public string FaultMessage { get; }
        }
    }
}