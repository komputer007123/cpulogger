﻿using System;
using System.Threading.Tasks;

namespace Cpulogger.Client.ServerApi
{
    public static class ApiExtensions
    {
        private const int AttemptNumber = 3;

        public static T RunFailSave<T>(this IApi api, Func<IApi, T> apiCall)
        {
            var failNumber = 0;
            while (true)
            {
                try
                {
                    return apiCall(api);
                }
                catch (Exception)
                {
                    failNumber++;
                    if (failNumber <= AttemptNumber)
                        continue;
                    throw;
                }
            }
        }

        public static async Task<T> RunFailSave<T>(this IApi api, Func<IApi, Task<T>> apiCall)
        {
            var failNumber = 0;
            while (true)
            {
                try
                {
                    return await apiCall(api);
                }
                catch (Exception)
                {
                    failNumber++;
                    if (failNumber <= AttemptNumber)
                        continue;
                    throw;
                }
            }
        }
    }
}