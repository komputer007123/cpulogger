using System.Collections;
using System.Linq;
using System.Threading.Tasks;

namespace Cpulogger.Client.ServerApi
{
    internal sealed class XmlRpcApi : IApi
    {
        private readonly XmlRpc _xmlRpc;

        public XmlRpcApi()
        {
            _xmlRpc = new XmlRpc("http://cpulogger.esy.es/xml.php");
        }

        public async Task<int> Login(string login, string password)
        {
            return (int) await _xmlRpc.CallAsync("login", login, password);
        }

        public async Task<int> Register(string login, string password)
        {
            return (int) await _xmlRpc.CallAsync("register", login, password);
        }

        public async Task<int> AddInfo(string[] auth, string computerName, string beginDate, string endDate, int interval, int[] cpuUsageArray)
        {
            return (int) await _xmlRpc.CallAsync("add_info", new ArrayList(auth), computerName,
                beginDate, endDate, interval, new ArrayList(cpuUsageArray));
        }

        public async Task<bool> DeleteAllCpuInfo(string[] auth)
        {
            return (bool) await _xmlRpc.CallAsync("delete_all_cpu_info", new ArrayList(auth));
        }

        public async Task<string[][]> GetAllNames()
        {
            return ((ArrayList) await _xmlRpc.CallAsync("get_all_names")).Cast<ArrayList>()
                .Select(t => t.Cast<string>().ToArray())
                .ToArray();
        }

        public async Task<string[][]> GetInfoList(int idUser, string computerName)
        {
            return ((ArrayList)await _xmlRpc.CallAsync("get_list", idUser, computerName)).Cast<ArrayList>()
                .Select(t => t.Cast<string>().ToArray())
                .ToArray();
        }

        public async Task<string[][]> GetInfo(int idCpuBlock)
        {
            return ((ArrayList)await _xmlRpc.CallAsync("get_info", idCpuBlock)).Cast<ArrayList>()
                .Select(t => t.Cast<string>().ToArray())
                .ToArray();
        }

        public Task<object> Eho(object @object)
        {
            return _xmlRpc.CallAsync("eho", @object);
        }
    }
}