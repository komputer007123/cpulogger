using System.Threading.Tasks;

namespace Cpulogger.Client.ServerApi
{
    public interface IApi
    {
        Task<int> Login(string login, string password);
        
        Task<int> Register(string login, string password);
        
        Task<int> AddInfo(string[] auth, string computerName, string beginDate, string endDate,
            int interval, int[] cpuUsageArray);
        
        Task<bool> DeleteAllCpuInfo(string[] auth);
        
        Task<string[][]> GetAllNames();
        
        Task<string[][]> GetInfoList(int idUser, string computerName);

        Task<string[][]> GetInfo(int idCpuBlock);
        
        Task<object> Eho(object @object);
    }

    //[XmlRpcUrl("http://cpulogger.esy.es/xml.php")]
    //public interface IApi : IXmlRpcProxy
    //{
    //    [XmlRpcMethod("login")]
    //    int Login(string login, string password);

    //    [XmlRpcMethod("register")]
    //    int Register(string login, string password);

    //    [XmlRpcMethod("add_info")]
    //    int AddInfo(string[] auth, string computerName, string beginDate, string endDate,
    //        int interval, int[] cpuUsageArray);

    //    [XmlRpcMethod("delete_all_cpu_info")]
    //    bool DeleteAllCpuInfo(string[] auth);

    //    [XmlRpcMethod("get_all_names")]
    //    string[][] GetAllNames();

    //    [XmlRpcMethod("get_list")]
    //    string[][] GetInfoList(int idUser, string computerName);

    //    [XmlRpcMethod("get_info")]
    //    string[][] GetInfo(int idCpuBlock);

    //    [XmlRpcMethod("eho")]
    //    object Eho(object @object);
    //}
}