namespace Cpulogger.Client.ServerApi
{
    public static class Api
    {
        public const string RegisterSuccess = @"success";
        public const string RegisterAlreadyExist = @"alreadyExist";

        public static IApi Instance { get; } = new XmlRpcApi();
    }
}