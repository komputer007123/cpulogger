﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Cpulogger.Client.Data;

namespace Cpulogger.Client.Services
{
    public sealed class RemoteService : Singleton<RemoteService>
    {
        public async Task<GetIpData> GetIp()
        {
            using (var handler = new HttpClientHandler { UseCookies = false })
            using (var hc = new HttpClient(handler))
            {
                hc.Timeout = new TimeSpan(0, 0, 30);
                var message = new HttpRequestMessage(HttpMethod.Get, new Uri(@"http://ip-api.com/json"));
                var msg = await hc.SendAsync(message);
                var json = await msg.Content.ReadAsStringAsync();

                var getIpData = JsonConvert.DeserializeObject<GetIpData>(json);
                return getIpData;
            }
        }

        /*
        {
            "status": "success",
            "country": "COUNTRY",
            "countryCode": "COUNTRY CODE",
            "region": "REGION CODE",
            "regionName": "REGION NAME",
            "city": "CITY",
            "zip": "ZIP CODE",
            "lat": LATITUDE,
            "lon": LONGITUDE,
            "timezone": "TIME ZONE",
            "isp": "ISP NAME",
            "org": "ORGANIZATION NAME",
            "as": "AS NUMBER / NAME",
            "query": "IP ADDRESS USED FOR QUERY"
        }

        {
            "status": "fail",
            "message": "ERROR MESSAGE",
            "query": "IP ADDRESS USED FOR QUERY"
        }
        */
    }
}
