using System;
using System.Linq;
using System.Threading.Tasks;
using Cpulogger.Client.Data;
using Cpulogger.Client.ServerApi;

namespace Cpulogger.Client.Services
{
    public sealed class CpuDataService : Singleton<CpuDataService>
    {
        public async Task<NameData[]> GetAllNamesAsync()
        {
            var result = await Api.Instance.RunFailSave(a => a.GetAllNames());
            if (result.Length == 0 || result[0].Length == 0)
                return new NameData[0];
            var currentUser = LoginService.Instance.IdUser;

            return result.Select(nameRow =>
            {
                var idUser = int.Parse(nameRow[1]);
                return new NameData(nameRow[0], idUser, idUser == currentUser);
            }).Distinct().ToArray();
        }

        public NameData[] GetAllNames()
        {
            return Task.Run(GetAllNamesAsync).Result;
        }

        public async Task<RecordData[]> GetGetRecordListAsync(NameData nameData)
        {
            var list = await Api.Instance.RunFailSave(a => a.GetInfoList(nameData.IdUser, nameData.Name));
            return list.Select(row => new RecordData(int.Parse(row[0]), row[1], row[2], int.Parse(row[3]))).ToArray();
        }

        public RecordData[] GetGetRecordList(NameData nameData)
        {
            return Task.Run(() => GetGetRecordListAsync(nameData)).Result;
        }

        public async Task<CpuUsageData> GetCpuUsageDataAsync(RecordData recordData)
        {
            var data = await Api.Instance.RunFailSave(a => a.GetInfo(recordData.IdRecord));
            var interval = int.Parse(data[0][0]);
            var cpuUsage = data.Skip(1).Select(t => int.Parse(t[0])).ToArray();
            return new CpuUsageData(interval, cpuUsage);
        }

        public CpuUsageData GetCpuUsageData(RecordData recordData)
        {
            return Task.Run(() => GetCpuUsageDataAsync(recordData)).Result;
        }

        public async Task SendCpuUsageDataAsync(DateTime beginRecordingTime, int interval, int[] cpuDataArray)
        {
            await Api.Instance.RunFailSave(
                a =>
                    a.AddInfo(LoginService.Instance.AuthData, Environment.MachineName,
                        DateTimeToString(beginRecordingTime),
                        DateTimeToString(DateTime.Now), interval, cpuDataArray));
        }

        public void SendCpuUsageData(DateTime beginRecordingTime, int interval, int[] cpuDataArray)
        {
            Task.Run(() => SendCpuUsageDataAsync(beginRecordingTime, interval, cpuDataArray)).Wait();
        }

        public async Task<bool> DeleteAllCpuInfoAsync()
        {
            return await Api.Instance.RunFailSave(api => api.DeleteAllCpuInfo(LoginService.Instance.AuthData));
        }

        public bool DeleteAllCpuInfo()
        {
            return Task.Run(DeleteAllCpuInfoAsync).Result;
        }

        private static string DateTimeToString(DateTime dateTime)
        {
            return
                $"{dateTime.Year}-{dateTime.Month:00}-{dateTime.Day:00} {dateTime.Hour:00}:{dateTime.Minute:00}:{dateTime.Second:00}";
        }
    }
}