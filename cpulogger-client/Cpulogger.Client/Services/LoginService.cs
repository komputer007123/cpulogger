﻿using System;
using System.Threading.Tasks;
using Cpulogger.Client.Data;
using Cpulogger.Client.ServerApi;

namespace Cpulogger.Client.Services
{
    public sealed class LoginService : Singleton<LoginService>
    {
        private User _user;

        public LoginService()
        {
            _user = Database.Instance.GetUser();
        }

        public bool IsUserExist => _user != null;
        public bool IsLogined => !string.IsNullOrWhiteSpace(_user?.Password);
        public int IdUser => _user.IdUser;
        public string Login => _user.Login;
        public string Password => _user.Password;
        public string[] AuthData => new[] {_user.Login, _user.Password};

        public async Task<bool> DoLogin(string login, string password)
        {
            if (string.IsNullOrWhiteSpace(login)) throw new ArgumentException(nameof(login));
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException(nameof(password));

            var idUser = await Api.Instance.Login(login, password);
            if (idUser <= 0)
                return false;
            _user = new User {IdUser = idUser, Login = login, Password = password};
            Database.Instance.SetUser(_user);
            return true;
        }

        public void DoLogout()
        {
            if (_user != null)
            {
                _user.Password = null;
            }

            Database.Instance.SetUser(_user);
        }

        public async Task<bool> Register(string login, string password)
        {
            if (string.IsNullOrWhiteSpace(login)) throw new ArgumentException(nameof(login));
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException(nameof(password));

            var idUser = await Api.Instance.Register(login, password);

            if (idUser <= 0)
                return false;
            _user = new User { IdUser = idUser, Login = login, Password = password };
            Database.Instance.SetUser(_user);
            return true;
        }
    }
}