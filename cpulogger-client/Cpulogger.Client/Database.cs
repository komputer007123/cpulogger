using System;
using System.IO;
using SQLite.Net;
using Cpulogger.Client.Data;

namespace Cpulogger.Client
{
    public sealed class Database : Singleton<Database>
    {
        private static readonly string DbPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), @"cpulogger.db");
        private readonly SQLiteConnectionWithLock _connection;

        public Database()
        {
            var sqlitePlatform =
#if __ANDROID__
                new SQLite.Net.Platform.XamarinAndroid.SQLitePlatformAndroid();
#else
                new SQLite.Net.Platform.Win32.SQLitePlatformWin32();
#endif
            _connection = new SQLiteConnectionWithLock(sqlitePlatform,
                new SQLiteConnectionString(DbPath, true));
            _connection.CreateTable<User>();
        }

        public User GetUser()
        {
            using (_connection.Lock())
            {
                return _connection.Table<User>().FirstOrDefault();
            }
        }

        public void SetUser(User user)
        {
            using (_connection.Lock())
            {
                _connection.RunInTransaction(() =>
                {
                    _connection.DeleteAll<User>();
                    if (user != null)
                        _connection.Insert(user);
                });
            }
        }
    }
}