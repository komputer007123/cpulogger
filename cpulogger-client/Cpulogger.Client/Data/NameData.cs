namespace Cpulogger.Client.Data
{
    public sealed class NameData
    {
        public NameData(string name, int idUser, bool thisMe)
        {
            Name = name;
            IdUser = idUser;
            ThisMe = thisMe;
        }

        public bool ThisMe { get; }

        public int IdUser { get; }

        public string Name { get; }

        public override bool Equals(object obj)
        {
            return obj is NameData && ((NameData)obj).IdUser == IdUser && ((NameData)obj).Name == Name;
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode() ^ IdUser.GetHashCode();
        }

        public override string ToString()
        {
            return $"{Name}{(ThisMe ? " - �" : string.Empty)}";
        }
    }
}