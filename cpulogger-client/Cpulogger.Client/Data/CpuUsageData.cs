namespace Cpulogger.Client.Data
{
    public sealed class CpuUsageData
    {
        public int Interval { get; }
        public int[] CpuUsage { get; }

        public CpuUsageData(int interval, int[] cpuUsage)
        {
            Interval = interval;
            CpuUsage = cpuUsage;
        }
    }
}