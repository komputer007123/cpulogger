﻿namespace Cpulogger.Client.Data
{
    public sealed class GetIpData
    {
        public string Query { get; set; }
    }
}