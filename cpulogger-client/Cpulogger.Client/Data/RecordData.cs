namespace Cpulogger.Client.Data
{
    public sealed class RecordData
    {
        public int IdRecord { get; }
        public string BeginTime { get; }
        public string EndTime { get; }
        public int IdUser { get; }

        public RecordData(int idRecord, string beginTime, string endTime, int idUser)
        {
            IdRecord = idRecord;
            BeginTime = beginTime;
            EndTime = endTime;
            IdUser = idUser;
        }

        public override string ToString()
        {
            return $"{BeginTime} --- {EndTime}";
        }
    }
}