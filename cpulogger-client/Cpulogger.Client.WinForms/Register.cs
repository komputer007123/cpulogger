﻿using Cpulogger.Client.Services;
using System;
using System.Windows.Forms;

namespace Cpulogger.Client.WinForms
{
    public partial class Register : Form
    {
        public Register()
        {
            InitializeComponent();
        }

        private async void registerButton_Click(object sender, EventArgs e)
        {
            var login = loginTextBox.Text;
            var password = passwordTextBox.Text;

            errorProvider.SetError(loginTextBox, string.IsNullOrWhiteSpace(login) ? "Поле не может быть пустым" : null);
            errorProvider.SetError(passwordTextBox, string.IsNullOrWhiteSpace(password) ? "Поле не может быть пустым" : null);
            if (!string.IsNullOrWhiteSpace(errorProvider.GetError(loginTextBox)) ||
                !string.IsNullOrWhiteSpace(errorProvider.GetError(passwordTextBox)))
                return;

            this.SetEnabledForAll(false);

            var isRegistered = await LoginService.Instance.Register(login, password);

            this.SetEnabledForAll(true);
            if (isRegistered)
            {
                errorProvider.SetError(registerButton, null);
                MessageBox.Show(@"Регистрация прошла успешно", @"Регистрация", MessageBoxButtons.OK);
                ((Login)Owner).SetLoginAndPassword(login, password);
                Close();
            }
            else
            {
                errorProvider.SetError(registerButton, "Пользователь с такин логин уже существует");
            }
        }
    }
}
