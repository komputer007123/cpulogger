﻿using Cpulogger.Client.Services;
using System;
using System.Windows.Forms;

namespace Cpulogger.Client.WinForms
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
            if (LoginService.Instance.IsUserExist)
            {
                loginTextBox.Text = LoginService.Instance.Login;
            }
        }

        public void SetLoginAndPassword(string login, string password)
        {
            loginTextBox.Text = login;
            passwordTextBox.Text = password;
        }

        private async void loginButton_Click(object sender, EventArgs e)
        {
            var login = loginTextBox.Text;
            var password = passwordTextBox.Text;

            errorProvider.SetError(loginTextBox, string.IsNullOrWhiteSpace(login) ? "Поле не может быть пустым" : null);
            errorProvider.SetError(passwordTextBox, string.IsNullOrWhiteSpace(password) ? "Поле не может быть пустым" : null);
            if (!string.IsNullOrWhiteSpace(errorProvider.GetError(loginTextBox)) ||
                !string.IsNullOrWhiteSpace(errorProvider.GetError(passwordTextBox)))
                return;

            this.SetEnabledForAll(false);

            var isLogined = await LoginService.Instance.DoLogin(login, password);

            this.SetEnabledForAll(true);
            if (isLogined)
            {
                errorProvider.SetError(loginButton, null);
                this.GoToForm(new MainForm());
            }
            else
            {
                errorProvider.SetError(loginButton, "Неверный логин или пароль");
            }
        }

        private void signinButton_Click(object sender, EventArgs e)
        {
            new Register().Show(this);
        }
    }
}
