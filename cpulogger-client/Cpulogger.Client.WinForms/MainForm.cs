﻿using Cpulogger.Client.Data;
using Cpulogger.Client.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cpulogger.Client.WinForms
{
    public partial class MainForm : Form
    {
        private readonly PerformanceCounter _cpucounter = new PerformanceCounter("Processor", "% Processor Time", "_Total");
        private bool _cpuRecordingStarted;
        private DateTime _beginRecordingTime;
        private readonly List<int> _cpuDataArray = new List<int>();
        private const int Interval = 1;

        public MainForm()
        {
            InitializeComponent();
        }

        private void allNamesComboBox_DropDown(object sender, EventArgs e)
        {
            var names = CpuDataService.Instance.GetAllNames();
            allNamesComboBox.Items.Clear();
            foreach (var name in names)
            {
                allNamesComboBox.Items.Add(name);
            }
        }

        private void listComboBox_DropDown(object sender, EventArgs e)
        {
            if (allNamesComboBox.Items.Count == 0 || allNamesComboBox.SelectedItem == null)
                return;
            var list = CpuDataService.Instance.GetGetRecordList((NameData) allNamesComboBox.SelectedItem);
            listComboBox.Items.Clear();
            foreach (var record in list)
            {
                listComboBox.Items.Add(record);
            }
        }

        private void listComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var data = CpuDataService.Instance.GetCpuUsageData((RecordData) listComboBox.SelectedItem);
            var interval = data.Interval;

            chart.Series["CPU %"].Points.Clear();

            for (var i = 0; i < data.CpuUsage.Length; ++i)
            {
                chart.Series["CPU %"].Points.AddXY(i*interval, data.CpuUsage[i]);
            }
        }

        private async void startButton_Click(object sender, EventArgs e)
        {
            stopButton.Enabled = true;
            this.SetEnabledForAll(false, true, chart, stopButton);
            chart.Series["CPU %"].Points.Clear();
            _cpuDataArray.Clear();
            _beginRecordingTime = DateTime.Now;
            _cpuRecordingStarted = true;

            var i = 0;
            while (_cpuRecordingStarted)
            {
                var curCpu = 0;
                await Task.Factory.StartNew(() =>
                {
                    curCpu = (int)_cpucounter.NextValue();
                    _cpuDataArray.Add(curCpu);
                });
                chart.Series["CPU %"].Points.AddXY(i, curCpu);
                await Task.Delay(Interval*1000);
            }
            await WhenStoped();
        }

        private async Task WhenStoped()
        {
            await CpuDataService.Instance.SendCpuUsageDataAsync(_beginRecordingTime, Interval, _cpuDataArray.ToArray());
        }

        private void stopButton_Click(object sender, EventArgs e)
        {
            _cpuRecordingStarted = false;
            stopButton.Enabled = false;
            this.SetEnabledForAll(true, true, chart, stopButton);
        }

        private void logoutButton_Click(object sender, EventArgs e)
        {
            this.SetEnabledForAll(false);
            LoginService.Instance.DoLogout();
            this.GoToForm(new Login());
        }

        private async void getIpButton_Click(object sender, EventArgs e)
        {
            this.SetEnabledForAll(false, true, stopButton);

            var ip = await RemoteService.Instance.GetIp();
            ipTextBox.Text = ip.Query;

            this.SetEnabledForAll(true, true, stopButton);
        }

        private async void removeAllButton_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, @"Вы уверены, что хотите удалить все свои данные?", @"Удаление всех данных",
                MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this.SetEnabledForAll(false, true, stopButton);

                allNamesComboBox.Items.Clear();
                listComboBox.Items.Clear();
                chart.Series["CPU %"].Points.Clear();

                await CpuDataService.Instance.DeleteAllCpuInfoAsync();

                this.SetEnabledForAll(true, true, stopButton);
            }
        }
    }
}
