﻿using Cpulogger.Client.Services;
using System;
using System.Windows.Forms;

namespace Cpulogger.Client.WinForms
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            if (LoginService.Instance.IsLogined)
            {
                Application.Run(new MainForm());
            }
            else
            {
                Application.Run(new Login());
            }
        }
    }
}
