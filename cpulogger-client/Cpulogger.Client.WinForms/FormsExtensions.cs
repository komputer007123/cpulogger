﻿using System.Linq;
using System.Windows.Forms;

namespace Cpulogger.Client.WinForms
{
    public static class FormsExtensions
    {
        public static void GoToForm(this Form @this, Form toOpen)
        {
            @this.Hide();
            toOpen.Closed += (s, args) => @this.Close();
            toOpen.Show();
        }

        public static void SetEnabledForAll(this Control @this, bool enabled, bool withoutContainers = true, params Control[] insteadOf)
        {
            if (insteadOf.Contains(@this))
                return;
            if (!withoutContainers || @this.Controls.Count == 0)
                @this.Enabled = enabled;
            foreach (Control control in @this.Controls)
            {
                SetEnabledForAll(control, enabled, withoutContainers, insteadOf);
            }
        }
    }
}