﻿using System;
using Android.App;
using Android.OS;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using Cpulogger.Client.Services;

namespace Cpulogger.Client.Droid
{
    [Activity(Label = "Авторизация", NoHistory = true, Icon = "@drawable/icon")]
    public class LoginActivity : FragmentActivity
    {
        private EditText _loginEditText;
        private EditText _passwordEditText;
        private Button _loginButton;
        private Button _registerButton;
        private View _root;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            RequestWindowFeature(WindowFeatures.NoTitle);
            SetContentView(Resource.Layout.login);

            _root = FindViewById(Resource.Id.root);
            _loginEditText = FindViewById<EditText>(Resource.Id.loginEditText);
            _passwordEditText = FindViewById<EditText>(Resource.Id.passwordEditText);
            _loginButton = FindViewById<Button>(Resource.Id.loginButton);
            _registerButton = FindViewById<Button>(Resource.Id.registerButton);

            _loginButton.Click += LoginButtonOnClick;
            _registerButton.Click += RegisterButtonOnClick;

            if (LoginService.Instance.IsUserExist)
            {
                _loginEditText.Text = LoginService.Instance.Login;
            }
        }

        private async void RegisterButtonOnClick(object sender, EventArgs eventArgs)
        {
            var login = _loginEditText.Text;
            var password = _passwordEditText.Text;

            _loginEditText.Error = string.IsNullOrWhiteSpace(login) ? "Поле не может быть пустым" : null;
            _passwordEditText.Error = string.IsNullOrWhiteSpace(password) ? "Поле не может быть пустым" : null;
            if (!string.IsNullOrWhiteSpace(_loginEditText.Error) ||
                !string.IsNullOrWhiteSpace(_passwordEditText.Error))
                return;

            _root.SetEnabledForAll(false);

            var isRegistered = await LoginService.Instance.Register(login, password);

            _root.SetEnabledForAll(true);
            if (isRegistered)
            {
                _registerButton.Error = null;
                Toast.MakeText(this, @"Регистрация прошла успешно", ToastLength.Long).Show();
                LoginButtonOnClick(sender, eventArgs);
            }
            else
            {
                var error = "Пользователь с такин логин уже существует";
                Toast.MakeText(this, error, ToastLength.Long).Show();
                _registerButton.Error = error;
            }
        }

        private async void LoginButtonOnClick(object sender, EventArgs eventArgs)
        {
            var login = _loginEditText.Text;
            var password = _passwordEditText.Text;

            _loginEditText.Error = string.IsNullOrWhiteSpace(login) ? "Поле не может быть пустым" : null;
            _passwordEditText.Error = string.IsNullOrWhiteSpace(password) ? "Поле не может быть пустым" : null;
            if (!string.IsNullOrWhiteSpace(_loginEditText.Error) ||
                !string.IsNullOrWhiteSpace(_passwordEditText.Error))
                return;

            _root.SetEnabledForAll(false);

            var isLogined = await LoginService.Instance.DoLogin(login, password);

            _root.SetEnabledForAll(true);
            if (isLogined)
            {
                _loginButton.Error = null;
                StartActivity(typeof(MainActivity));
            }
            else
            {
                var error = "Неверный логин или пароль";
                Toast.MakeText(this, error, ToastLength.Long).Show();
                _loginButton.Error = error;
            }
        }
    }
}