﻿using System.Linq;
using Android.Views;

namespace Cpulogger.Client.Droid
{
    public static class ViewExtensions
    {
        public static void SetEnabledForAll(this View @this, bool enabled, bool withoutContainers = true, params View[] insteadOf)
        {
            if (insteadOf.Contains(@this))
                return;
            var viewGroup = @this as ViewGroup;
            if (viewGroup == null)
            {
                @this.Enabled = enabled;
            }
            else
            {
                if (!withoutContainers)
                    @this.Enabled = enabled;
                for (var i = 0; i < viewGroup.ChildCount; i++)
                {
                    SetEnabledForAll(viewGroup.GetChildAt(i), enabled, withoutContainers, insteadOf);
                }
            }
        }
    }
}