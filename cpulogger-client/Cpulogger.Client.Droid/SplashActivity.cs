﻿using Android.App;
using Android.OS;
using Android.Support.V4.App;
using Android.Views;
using Cpulogger.Client.Services;

namespace Cpulogger.Client.Droid
{
    [Activity(Label = "Cpulogger", MainLauncher = true, NoHistory = true, Icon = "@drawable/icon")]
    public class SplashActivity : FragmentActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            RequestWindowFeature(WindowFeatures.NoTitle);
            //Window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            //    WindowManager.LayoutParams.FLAG_FULLSCREEN);

            if (LoginService.Instance.IsLogined)
            {
                StartActivity(typeof(MainActivity));
            }
            else
            {
                StartActivity(typeof(LoginActivity));
            }
        }
    }
}