﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Android.App;
using Android.OS;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using Cpulogger.Client.Data;
using Cpulogger.Client.Services;
using Java.IO;
using Java.Lang;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using OxyPlot.Xamarin.Android;
using String = Java.Lang.String;

namespace Cpulogger.Client.Droid
{
    [Activity(Label = "Cpulogger", Icon = "@drawable/icon")]
    public class MainActivity : FragmentActivity
    {
        private bool _cpuRecordingStarted;
        private DateTime _beginRecordingTime;
        private readonly List<int> _cpuDataArray = new List<int>();
        private const int Interval = 1;
        private PlotView _graph;
        private View _root;
        private Spinner _allNamesSpinner;
        private Spinner _listSpinner;
        private Button _startButton;
        private Button _stopButton;
        private Button _removeAllButton;
        private Button _logoutButton;
        private Button _getIpButton;
        private EditText _ipEditText;
        private ProgressDialog _progressDialog;
        private NameData[] _allNames;
        private RecordData[] _recordList;
        private CpuUsageData _data;
        private int _interval;
        private LineSeries _series;
        private ArrayAdapter<String> _allNamesAdapter;
        private ArrayAdapter<String> _listSpinnerAdapter;

        protected override async void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            
            SetContentView(Resource.Layout.main);

            _progressDialog = new ProgressDialog(this);
            _progressDialog.SetCancelable(false);

            _root = FindViewById(Resource.Id.root);
            _allNamesSpinner = FindViewById<Spinner>(Resource.Id.allNamesSpinner);
            _listSpinner = FindViewById<Spinner>(Resource.Id.listSpinner);
            _graph = FindViewById<PlotView>(Resource.Id.graph);
            _startButton = FindViewById<Button>(Resource.Id.startButton);
            _stopButton = FindViewById<Button>(Resource.Id.stopButton);
            _removeAllButton = FindViewById<Button>(Resource.Id.removeAllButton);
            _logoutButton = FindViewById<Button>(Resource.Id.logoutButton);
            _getIpButton = FindViewById<Button>(Resource.Id.getIpButton);
            _ipEditText = FindViewById<EditText>(Resource.Id.ipEditText);
            
            _allNamesSpinner.ItemSelected += AllNamesSpinnerOnItemSelected;
            _listSpinner.ItemSelected += ListSpinnerOnItemSelected;
            _startButton.Click += StartButtonOnClick;
            _stopButton.Click += StopButtonOnClick;
            _getIpButton.Click += GetIpButtonOnClick;
            _logoutButton.Click += LogoutButtonOnClick;
            _removeAllButton.Click += RemoveAllButtonOnClick;

            _graph.Model = CreatePlotModel();

            await LoadAllNames();
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.main_menu, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.update:
                    LoadAllNames();
                    return true;
                default:
                    return base.OnOptionsItemSelected(item);
            }
        }

        private async Task LoadAllNames()
        {
            _progressDialog.SetMessage("Загрузка списка компьютеров...");
            _progressDialog.Show();
            try
            {
                _allNames = await CpuDataService.Instance.GetAllNamesAsync();
            }
            finally
            {
                _progressDialog.Dismiss();
            }
            _allNamesAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerItem,
                _allNames.Select(t => new String(t.ToString())).ToArray());
            _allNamesAdapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            _allNamesSpinner.Adapter = _allNamesAdapter;
        }

        private void RemoveAllButtonOnClick(object sender, EventArgs eventArgs)
        {
            new AlertDialog.Builder(this)
                .SetTitle(@"Удаление всех данных")
                .SetMessage(@"Вы уверены, что хотите удалить все свои данные?")
                .SetPositiveButton("Да", async (o, args) =>
                {
                    _root.SetEnabledForAll(false, true, _stopButton);

                    _allNamesSpinner.Adapter = null;
                    _listSpinner.Adapter = null;
                    _series.Points.Clear();
                    _graph.InvalidatePlot();

                    await CpuDataService.Instance.DeleteAllCpuInfoAsync();

                    _root.SetEnabledForAll(true, true, _stopButton);
                })
                .SetNegativeButton("Нет", (o, args) => { }).Show();
        }

        private void LogoutButtonOnClick(object sender, EventArgs eventArgs)
        {
            _root.SetEnabledForAll(false);
            LoginService.Instance.DoLogout();
            StartActivity(typeof(LoginActivity));
            Finish();
        }

        private async void GetIpButtonOnClick(object sender, EventArgs eventArgs)
        {
            _root.SetEnabledForAll(false, true, _stopButton);

            var ip = await RemoteService.Instance.GetIp();
            _ipEditText.Text = ip.Query;

            _root.SetEnabledForAll(true, true, _stopButton);
        }

        private PlotModel CreatePlotModel()
        {
            var plotModel = new PlotModel { Title = "CPU %" };

            plotModel.Axes.Add(new LinearAxis { Position = AxisPosition.Bottom });
            plotModel.Axes.Add(new LinearAxis { Position = AxisPosition.Left/*, Maximum = 100, Minimum = 0 */});

            _series = new LineSeries
            {
                MarkerType = MarkerType.Circle,
                MarkerSize = 4,
                MarkerStroke = OxyColors.White
            };
            plotModel.Series.Add(_series);

            return plotModel;
        }

        private void StopButtonOnClick(object sender, EventArgs eventArgs)
        {
            _cpuRecordingStarted = false;
            _stopButton.Enabled = false;
            _root.SetEnabledForAll(true, true, _graph, _stopButton);
        }

        private async void StartButtonOnClick(object sender, EventArgs eventArgs)
        {
            _stopButton.Enabled = true;
            _root.SetEnabledForAll(false, true, _graph, _stopButton);
            _cpuDataArray.Clear();
            _series.Points.Clear();
            _beginRecordingTime = DateTime.Now;
            _cpuRecordingStarted = true;

            var i = 0;
            while (_cpuRecordingStarted)
            {
                var curCpu = 0;
                await Task.Factory.StartNew(() =>
                {
                     curCpu = (int)ReadUsage();
                    _cpuDataArray.Add(curCpu);
                });
                _series.Points.Add(new DataPoint(i * _interval, curCpu));
                _graph.InvalidatePlot();
                await Task.Delay(Interval * 1000 - 360);
                ++i;
            }
            await WhenStoped();
        }

        private async Task WhenStoped()
        {
            await CpuDataService.Instance.SendCpuUsageDataAsync(_beginRecordingTime, Interval, _cpuDataArray.ToArray());
        }
        
        private async void ListSpinnerOnItemSelected(object sender, AdapterView.ItemSelectedEventArgs itemSelectedEventArgs)
        {
            await LoadRecord(itemSelectedEventArgs.Position);
        }

        private async Task LoadRecord(int position)
        {
            _progressDialog.SetMessage("Загрузка записи...");
            _progressDialog.Show();
            try
            {
                var record = _recordList[position];
                _data = await CpuDataService.Instance.GetCpuUsageDataAsync(record);
                _interval = _data.Interval;
            }
            finally
            {
                _progressDialog.Dismiss();
            }
            _series.Points.Clear();
            _series.Points.AddRange(_data.CpuUsage.Select((t, i) => new DataPoint(i*_interval, t)).ToArray());
            _graph.InvalidatePlot();
        }

        private async void AllNamesSpinnerOnItemSelected(object sender, AdapterView.ItemSelectedEventArgs itemSelectedEventArgs)
        {
            await LoadRecordList(itemSelectedEventArgs.Position);
        }

        private async Task LoadRecordList(int position)
        {
            var name = _allNames[position];

            _progressDialog.SetMessage("Загрузка списка записей...");
            _progressDialog.Show();
            try
            {
                _recordList = await CpuDataService.Instance.GetGetRecordListAsync(name);
            }
            finally
            {
                _progressDialog.Dismiss();
            }
            _listSpinnerAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleSpinnerItem,
                _recordList.Select(t => new String(t.ToString())).ToArray());
            _listSpinnerAdapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            _listSpinner.Adapter = _listSpinnerAdapter;
        }

        private static float ReadUsage()
        {
            try
            {
                var reader = new RandomAccessFile("/proc/stat", "r");
                var load = reader.ReadLine();

                var toks = load.Split(' ');

                var idle1 = long.Parse(toks[5]);
                var cpu1 = long.Parse(toks[2]) + long.Parse(toks[3]) + long.Parse(toks[4])
                      + long.Parse(toks[6]) + long.Parse(toks[7]) + long.Parse(toks[8]);

                try
                {
                    Thread.Sleep(360);
                }
                catch (Java.Lang.Exception)
                {
                }

                reader.Seek(0);
                load = reader.ReadLine();
                reader.Close();

                toks = load.Split(' ');

                var idle2 = long.Parse(toks[5]);
                var cpu2 = long.Parse(toks[2]) + long.Parse(toks[3]) + long.Parse(toks[4])
                    + long.Parse(toks[6]) + long.Parse(toks[7]) + long.Parse(toks[8]);

                return (float)(cpu2 - cpu1) / ((cpu2 + idle2) - (cpu1 + idle1)) * 100f;

            }
            catch (IOException)
            {
                //ex.printStackTrace();
            }

            return 0;
        }

        //#region MyRegion

        //private int[] getCpuUsageStatistic()
        //{

        //    string tempString = executeTop();

        //    tempString = tempString.Replace(",", "");
        //    tempString = tempString.Replace("User", "");
        //    tempString = tempString.Replace("System", "");
        //    tempString = tempString.Replace("IOW", "");
        //    tempString = tempString.Replace("IRQ", "");
        //    tempString = tempString.Replace("%", "");
        //    for (int i = 0; i < 10; i++)
        //    {
        //        tempString = tempString.Replace("  ", " ");
        //    }
        //    tempString = tempString.Trim();
        //    string[] myString = tempString.Split(' ');
        //    int[] cpuUsageAsInt = new int[myString.Length];
        //    for (int i = 0; i < myString.Length; i++)
        //    {
        //        myString[i] = myString[i].Trim();
        //        cpuUsageAsInt[i] = int.Parse(myString[i]);
        //    }
        //    return cpuUsageAsInt;
        //}

        //private string executeTop()
        //{
        //    Process p = null;
        //    BufferedReader @in = null;
        //    string returnString = null;
        //    try
        //    {
        //        p = Runtime.GetRuntime().Exec("dumpsys cpuinfo");
        //        @in = new BufferedReader(new InputStreamReader(p.InputStream));
        //        while (string.IsNullOrEmpty(returnString))
        //        {
        //            returnString = @in.ReadLine();
        //        }
        //    }
        //    catch (IOException e)
        //    {
        //        //Log.e("executeTop", "error in getting first line of top");
        //        //e.printStackTrace();
        //    }
        //    finally
        //    {
        //        try
        //        {
        //            @in?.Close();
        //            p?.Destroy();
        //        }
        //        catch (IOException e)
        //        {
        //            //Log.e("executeTop",
        //            //        "error in closing and destroying top process");
        //            //e.printStackTrace();
        //        }
        //    }
        //    return returnString;
        //}

        //#endregion
    }
}


